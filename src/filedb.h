#ifndef FILEDB_H
#define FILEDB_H

#include "strings.h"

#define byte char

#define bool int
#define true 1
#define false 0

#define INT "int"
#define STR "str"
#define FLT "flt"

struct db_s {
    char db_name[64];
    int table_count;
    string* table_names;
};

typedef struct db_s db;

struct filedb_s {
    char* filename;
    db db;
};

typedef struct filedb_s filedb;

struct table_def_s {
    string* field_names;
    string* field_type;
    bool* field_null;
    string* field_default;
    int primary_key;
    int nr_fields;
};

typedef struct table_def_s table_def;

struct entry {
    void* data;
    table_def table;
};

typedef struct entry_s entry;

struct entry_list_s {
  entry* entries;
  int lenght;
};

typedef struct entry_list_s entry_list;

struct dql_error_s {
  int error_code;
  char* error_message;
};

typedef struct dql_error_s dql_error;

filedb open_filedb(char*);

filedb create_filedb(char*);

db add_db_table(db, table_def);

void close_filedb(filedb);

entry_list select_query(db, char*);

dql_error run_query(db, char*);




#endif
