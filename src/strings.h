#ifndef STRINGS_H
#define STRINGS_H

struct string_s {
  char* string;
  int length;
};

typedef struct string_s string;

#endif
