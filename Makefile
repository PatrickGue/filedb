CC=gcc
SRC_DIR=src
SRCS=${SRC_DIR}/*.c
BIN_DIR=bin
TEST_PROG=${BIN_DIR}/test
DOC_DIR=doc
MAKE_PROG=make


all:${TEST_PROG}

${TEST_PROG}:
	mkdir -p ${BIN_DIR}
	${CC} ${SRCS} -o ${TEST_PROG}

doc:
	${MAKE_PROG} -C ${DOC_DIR} doc

clean:
	rm -rf ${BIN_DIR}
	${MAKE_PROG} -C ${DOC_DIR} clean
